function(context, args)
{
	let L = #fs.scripts.lib()
	if(context.calling_script||context.is_scriptor) {return{ok:false, msg: "`A:::IMPLINK_COMMUNICATION:::` Messing with scripts is unlawful. Access denied."}} //prevents people poking with scripts
	if (!["implink", "imp", "imphunter", "omp"].includes(context.caller))
		return {ok:false, msg:"Get your own toolbox."}
	
	let home = [
	  "  _____________  Current toolkit:",
	  " | ___ ___ ___ | cmd:\"help\"",
	  " ||_=_|_=_|_=_|| cmd:\"check\", s:#s.a.s",
	  " ||____===____|| cmd:\"run\", a:{args}",
	  " ||____===____|| cmd:\"balance\"",
	  " ||____===____|| cmd:\"brute\", WIP",
	  " ||____===____|| cmd:\"alerts\"",
	  " ''-----------'' cmd:\"db\""
	].join("\n")

	if (!args || !args.cmd) 
		return home
	

	let command = args.cmd

	if (command == "help") {
		let info = [
			"`Vhelp`    `c-` returns this page.",
			"`Vcheck`   `c-` scripts sec and access level checker.",
			"`Vbalance` `c-` returns balances from jade.vita, dtr.haunty_mall and pay.pal",
			"`Vbrute`   `c-` brute force a pin (args WIP)",
			"`Vrun`     `c-` safety wrapper, override with confirm:true",
			"`Valerts`  `c-` show unread db alerts",
			"`Vdb`      `c-` search/delete/update entries run only cmd:\"db\" for details"
		].join("\n")
		return info
	}

	if (command == "balance") {
		function MatchStr(find, thing) 
		{
			return (find.match(thing) || [] ) [1]
		}

		let dtr = #fs.dtr.haunty_mall({ledger:"show"})
		let dtrmatch = MatchStr(dtr, /balance is ([^\n]+)/)
		if (dtrmatch == undefined) 
			dtrmatch = "0GC"
		let dtrsum = L.to_gc_num(dtrmatch)

		let pay = #fs.pay.pal()
		let paymatch = MatchStr(pay, /balance is ([^\.]+)/)
		let paynum = L.to_gc_num(paymatch)

		let jade = #fs.jade.vita()
		let jadematch = MatchStr(jade, /== ([^\n]+)/)
		let jadenum = L.to_gc_num(jadematch)
		
		let user = #hs.accts.balance()
		let total = paynum + jadenum + user + dtrsum

		let totalgc = L.to_gc_str(total)
		let bal = L.to_gc_str(user)

		let caller = "\ndtr balance is :      " + dtrmatch + " \n"
		let info = "\n`c(dtr balance is shared if you are connected on dtr``c.``cpublic_alts)`"
		if (!["implink", "imphunter", "imp"].includes(context.caller)) {
			caller = " \n"
			info = " "
		}

		let ret = [
			"pay.pal balance is:   " + paymatch + " ",
			"jade.vita balance is: " + jadematch + " ",
			"User balance is:      " + bal + " " + caller,
			"Total balance is:     " + totalgc + " " + info
		].join("\n")

		return ret
	}

	let check = args.s

	if (command == "check") {
    let check = args.s
    let p = " `DPRIVATE`"
    let t = ""
    let h = ""

	  function retMsg(sec)
	  {
	    let sl=["`dNULLSEC`","`DLOWSEC`","`FMIDSEC`","`HHIGHSEC`","`LFULLSEC`"];
	    return {ok:true, msg:check.name + " is " + sl[sec] + "" + p + h + t}
	  }

    let acc=#fs.scripts.get_access_level({name:check.name})
    let sec=#fs.scripts.get_level({name:check.name})


    if (acc.public)
      p = " `APUBLIC`"

    if (acc.hidden)
      h = " `AHIDDEN`" 

    if (acc.trust)
      t = " `FTRUST`"

    if(typeof sec=="number")
      return retMsg(sec);
    return {ok:false, msg:"Not a script."} 
  }

  if (command == "run") {
		let check = args.s

	  function retMsg(sec)
	  {
	    let sl=["`dNULLSEC`","`DLOWSEC`","`FMIDSEC`","`HHIGHSEC`","`LFULLSEC`"];
			return check.name + " is " + sl[sec] +", run this with confirm:true"
		}		

		let a = args.a
		let s = #fs.scripts.get_level({name:check.name})
		if (typeof s == "number" ){
			if (s == 4 || args.confirm == true){
				if (!args.a)
					return check.call()

				return check.call(args.a)
			}

			return retMsg(s)
		}  
		throw new Error("Not a script.")
	}

	if (command == "brute")
		return {ok:false, msg:"WIP"}

	if (command == "alerts")
		return {ok:false, msg:"WIP"}

	if (command == "db") {
	  let I = #fs.implink.lib()
		let home = [
			"   __",
			"  /-/|	Database Search",
			" /_/ |	* error logs  `c-` {cmd:\"db\", do:\"error_logs\", script:\"script\", user:\"username\"}",
			" | |/|	* logged data `c-` {cmd:\"db\", do:\"logged_data\", script:\"script\", user:\"username\"}",
			" |^| |	* custom      `c-` {cmd:\"db\", do:\"delete/update/search\", db:{search:\"here\"}}",
			" |_|/"
	 	].join("\n")

	  if (!args.do || args.do == "help")
	   	return home

	  function NoEntry(){
	  	return {ok:false, msg:"No entries."}
	  }

	  let cmd = args.do
	  let script_arg = args.script

	  if (cmd == "error_logs"){
	      let query={type:"message_log"};

	      if(script_arg)
	          query.script=script_arg;

	      let check = #db.f(query).array()

	      if (check == "")
	          return NoEntry()

	      return check
	  }

	  if (cmd == "logged_data") {
	    let query = {type:"logs"};

	    if (script_arg)
	      query.script = script_arg

	    if (args.user)
	      query.caller = args.user

	    let check = #db.f(query).limit(50).sort({date:1}).array()

	    if (check == "")
	      return NoEntry()

	    return check
	  }

	  let query = args.db

	  if (cmd == "delete") {
	    if (!args.confirm) {
	      let check = #db.f(query).array()

	      if (check == "")
	        return { ok:false, msg:"You are trying to delete nothing." }

	      return { ok:false, msg:"Confirm deletion of the following entries with confirm:true\n\n" + JSON.stringify(check, 0, 0, 2)}
	    }

	    return #db.r(query)
	  }

	  if (cmd == "update") {
	    let set = args.set

	    if (!args.confirm) {
	      let check = #db.f(query).array()

	      if (check == "")
	        return { ok:false, msg:"You are trying to update nothing." }

	      return { ok:false, msg:"Confirm updating the following entries with confirm:true\n\n" + JSON.stringify(check, 0, 0, 2) }
	    }
	    return #db.u1(query, {$set:set})
	  }

	  if (cmd == "search") {
	    if (!query)
	      return "missing arg `Ndb`"
	    let search = #db.f(query).limit(50).array()

	    if (search == "")
	      return NoEntry()

	    return search
	  }
	  
	  return home
	}
	return home
}
