function(context, args) //corp:#s.corp.name
{
	if (!args || !args.corp) 
		return "implink.brute {corp:#s.corp.name}"
	
	#db.r({type:"brute_t3", date:{$lte:Date.now()-8*3600*1000}}); //remove entries older than 8 hours
	// 8(hours) * 3600 (seconds in an hour) * 1000(turn seconds to miliseconds)
	//$lte = less than/equal to

	let user = #db.f({
		type:"brute_t3", 
		caller:context.caller,
		corp: args.corp.name,
	}).first() //check for existing brute entry

	if (!user) {
		user = {
			_id:#db.ObjectId(), //searching using _id for faster queries #db.ObjectId() is the default _id it was going to be assigned
			type:"brute_t3",
			corp: args.corp.name,
			caller: context.caller,
			date: Date.now(),
			found_user: false,
			username_index: 0,
			current_pin: -1 //set to -1 to ensure brute username first
		}
		#db.i(user)
	}

	let input = {}
	let corp = args.corp.call //we do let output = corp(input) to call the script and insert args

	if (user.current_pin == -1) { //if its -1 then we don't have the username
		let npclist = #db.f({type:"npc_usernames"}).first() //get list of npc usernames

		for (let x = user.username_index; x < npclist.usernames.length; x++) { //current username attempt < npc username list
			input.username = npclist.usernames[x] //arg username = current username attempt
			let output = corp(input) //call the script *once* in this loop
			//#D(input)
			if (output.includes("provide") || output.includes("text")) { //are we at insert pin section?
				user.current_pin = 0 //set current_pin to 0, now we can start bruting the pin
				#db.u1({_id:user._id},{$set:{current_pin:0, username_index:x, npc_username:npclist.usernames[x]}})
				//user._id, getting the _id from user variable
				//inserting key npc_username so we can call the username when bruting pin
				user.npc_username = npclist.usernames[x] 
				/*we do this because if there is no db entry when the user is first calling the script, 
				the variable user wont have npc_username as a key, so causes errors */

				break; //exit this loop
			}
			if(x % 10==0) // divide current index by 10, if remainder is 0, that means it has been 10 calls, update the current attempt
				#db.u1({_id:user._id},{$set:{username_index:x}}) 
				// we update every 10 times to cut down on db calls, which means faster bruting
		}
	}

	for (let x = user.current_pin; x < 10000; x++) { //current pin < 10000 (pin goes up to 9999)
		input.username = user.npc_username //if username was "imp" this would be username:"imp"
		input.pin = ("000"+x).slice(-4) //.slice(-4) gets the last 4 characters, so if x was 123, the result would be pin:"0123" since it's only taking the last 4 characters
		let output = corp(input) // call once
		//#D(input) //<- debugging
		if(!(output.includes('incorrect') || output.includes('pin'))) { //is "incorrect" or "pin" missing? need to test if this causes issues if pin/incorrect is corrupted
			#db.u1({_id:user._id},{$set:{current_pin:x}}) //update current pin using _id for faster query
			throw new Error("username:\""+user.npc_username+"\", pin:\"" + ("000"+x).slice(-4) + "\"") //throwing it as an error so it stops /auto
			break; //exit loop, this essentially does nothing since we're throwing an error
		}

		if(x % 10==0) // divide current index by 10, if remainder is 0, update the current attempt
			#db.u1({_id:user._id},{$set:{current_pin:x}})
		// we update every 10 times to cut down on db calls, which means faster bruting

		if (new Date() - _START > 4000) { //if script run is greater than 4000ms (we have a 5000ms runtime max)
			#db.u1({_id:user._id},{$set:{current_pin:x}}) //update to last attempt
			return {ok:true, msg:"pin not found, please rerun\nlast pin\u200b: " + ("000"+x).slice(-4)}
		}
	}
	throw new Error("something went wrong.") //happens if correct pin is missed, which should (hopefully) not happen
}
