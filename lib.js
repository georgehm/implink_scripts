function(context, args)
{
	let L = #fs.scripts.lib()
	if (context.calling_script == null){
		return "Functions: \n* logData(context.calling_script, context.is_scriptor, args)\n* errLog(\"error message\", context.calling_script, context.is_scriptor, args)"
	}

	if(context.calling_script.split(".")[0] != "implink" && context.calling_script.split(".")[0] != "paste" )
		return {ok:false}

	function StatusRet(array, type, msg){
		let runtime = Date.now() - _START
		let start = "[`ABEG`] "
		let update = "[`FUPD`] "
		let success = "[`LSUC`] "
		let error = "[`DERR`] "
		let end = "[`AEND`] "

		// switch (type) { //a switch doesn't work here but an if does? what?
		// 	case "start":
		// 		type = start
		// 	case "update": 
		// 		type = update
		// 	case "success":
		// 		type = success
		// 	case "error":
		// 		type = error
		// 	case "end":
		// 		type = end
		// }

		if (type == "start") {
			type = start
			// msg = "Starting " + context.calling_script
		}

		if (type == "update")
			type = update

		if (type == "success")
			type = success

		if (type == "error")
			type = error

		if (type == "end")
			type = end

		if (!type.includes("["))
			return false

		let space = ""
		if (runtime < 100)
			space = "  "
		if (runtime < 1000 && runtime > 99)
			space = " "
		array.push("`A" + runtime + "``cms`" + space + type + msg)

		return true
	}

	function Decorrupt(script,a) {
		let N=String.fromCharCode(0); //get a NULL character
	  if(typeof script=="object" && typeof script.call=="function") //is script arg a scriptor?
	  	script = script.call; //set script as script.call so we dont have to change anything for script arg later

		let base=script(a).replace(/`[A-Za-z0-9][^ -~]`/g,N).split('') //replace coloured corruption chars with NULL chatacter
		//put base as an array since we can't replace single characters in a string

		// `[A-Za-z0-9][^ -~]` 
		// ^                 ^ // literal backticks
		//  ^~~~~~~~~~^        // character class, containing...
		//   ^^^               //   A through Z...
		//      ^^^            //   or a through z...
		//         ^^^         //   or 0 through 9. i.e. all the color codes
		//             ^~~~~^  //   character class containing...
		//              ^      //   everything except...
		//               ^^^   //   space through ~, which is all printable ascii characters
		// https://regex101.com/r/dLljbB/3 
		//g is global, so do this with all matches

		while(base.includes(N)) { //while we have null characters in our array
			let last = -1 //last index we're checking for NULL char
			//setting the comparison string, we plan to call this later to reset our comparison string
			while(true) { //the loop where we're gonna replace NULL characters
				let ind=base.indexOf(N, last); //get the index of a NULL character from our intial string, starting from our last index
				if(ind==-1) //have we gone through the entire string and back to -1?
					break; // done, yay!

				base[ind]=compare[ind]  //set the original strings index to the compiarson string index, this should (hopefully) replace a NULL character
				last = ind //set the last index we did to well... the last index we did
			}
		}
	 	return base.join("") //return the decorrupted string
 	}

	function ConvertTimestamp(timestamp)
	{
		let date = new Date(timestamp).toUTCString()
		return date
	}

	function AlertError(msg, c_s, i_s, a, ret)
	{
		let insert = {
			type:"error",
			date: Date.now(),
			msg: msg,
			caller: context.caller,
			calling_script: c_s,
			is_scriptor: i_s,
			script: context.calling_script,
			args: a,
			unread: true,
			id: L.create_rand_string(5),
			ret_msg: ret
		}

		if (ret) {
			#db.i(insert)
			return {ok:false, msg:"`DCRYPTIC ERROR " + ret + " PLEASE NOTIFY IMPLINK`" }
		}
		ret_msg = null
		#db.i(insert)
	}

	function logData(c_s, i_s, a)
	{
		#db.i({
			type:"logs",
			date: Date.now(),
			caller: context.caller, //caller of the script
			calling_script: c_s, //set to context.calling_script when calling this function
			is_scriptor: i_s, //set to context.is_scriptor when calling this function (if we do it now it'd return false)
			script: context.calling_script, //script that uses this function
			args: a
		})
	}


	let format={}
	format.columns=function(dat,titles,{pre,sep,suf},blank) {
	    if(!Array.isArray(dat))return dat;
	    if(!pre && pre!=='')pre="\u2503 ";
	    if(!sep && sep!=='')sep=" \u2503 ";
	    if(!suf && suf!=='')suf=" \u2503";

	    if(typeof sep == "object") {
	        s=sep.sep||s;
	        p=sep.pre||p;
	        f=sep.suf||f;
	    }
	    if(typeof dat[0]=="object" && !Array.isArray(dat[0])) {
	        var d=dat;
	        dat=[];
	        for(var i=0;i<d.length;++i) {
	            var a=[];
	            for(var j=0;j<titles.length;++j) {
	                a.push(d[i][titles[j].key]);
	            }
	            dat.push(a);
	        }
	    }

	    dat=JSON.parse(JSON.stringify(dat))
	    var ls=[];
	    if(titles[0].name!==false) {
	        var a=[];
	        for(var j=0;j<titles.length;++j) {
	            a.push(titles[j].name);
	            ls.push(format.decolor(titles[j].name).length)
	        }
	        dat.unshift(a);
	    }
	    else {
	        for(var j=0;j<titles.length;++j) {
	            ls.push(0)
	        }
	    }

	    for(var i=1;i<dat.length;++i) {
	        for(var j=0;j<dat[i].length;++j) {
	            if(titles[j].func)
	                dat[i][j]=titles[j].func(dat[i][j]);
	            if(blank && !dat[i][j] && dat[i][j]!==0)dat[i][j]='';
	            var l=format.decolor(dat[i][j]).length;
	            if(l>ls[j])
	                ls[j]=l;
	            if(titles[j].max_length && titles[j].max_length<ls[j])
	                ls[j]=titles[j].max_length;
	        }
	    }

	    for(var i=0;i<dat.length;++i)
	        for(var j=0;j<dat[i].length;++j) {
	            dat[i][j]=format.pad(dat[i][j],ls[j],titles[j].dir,' ',titles[j].max_length);
	        }
	    for(var i=0;i<dat.length;++i) {
	        dat[i]=dat[i].join(sep);
	    }
	    dat=dat.join(suf+"\n"+pre);
	    return "\n"+pre+dat+suf;
	}

	format.pad=function(s,l,dir,c,trunc) {
	    s+="";
	    if(!c)c=' ';
	    c+="";
	    if(trunc && s.length>l)
	        s=s.substring(0,l);
	    if(c.length>1)c=c[0];
	    if(!c.length)c=' ';
	    if(typeof dir == "undefined")dir=1;

	    var d=format.decolor(s);

	    var num=l-d.length;
	    if(dir==0) {
	        for(var i=0;i<num;++i)
	            s=(i%2==0?' ':'')+s+(i%2?' ':'');
	        return s;
	    }
	    else {
	        var p='';
	        for(var i=0;i<num;++i)
	            p+=c;
	        if(dir>0)
	            return s+p;
	        return p+s;
	    }
	}

	format.decolor=function(s) {
	    s+="";
	    return s.replace(/`[0-9A-Za-z](?!:.?`)([^`\n]+)`/g,"$1").replace(/\u200B/g,""); // "])) // syntax
	}

	format.formatTime=function(s) {
        var pref='';
        if(s<0) {
            s=-s;
            pref='-';
        }
        if(s==0) {
            return _p(s).replace(/00/g,'0')+'`Cs`';
        }
        var sec=s%60;
        s=Math.floor(s/60);
        var min=s%60;
        s=Math.floor(s/60);
        var h=s%24;
        s=Math.floor(s/24);
        var d=s;
        var ret=''

        function _p(s) {
            if(s==0)
                return '00';
            if(s<10)
                return '0'+s;
            return s+'';
        }

        if(d) {
            h=_p(h);
            min=_p(min)
            sec=_p(sec);
        }
        else if(h) {
            min=_p(min)
            sec=_p(sec);
        }
        else if(min) {
            sec=_p(sec);
        }
        else if(!s) {
            s='0';
        }
        if(!sec)sec=_p(0);

        if(d)ret+=pref+d+'`5d`';
        if(h)ret+=(d?'':pref)+h+'`2h`';
        if(min)ret+=(d||h?'':pref)+min+'`1m`';
        if(sec)ret+=(d||h||min?'':pref)+sec+'`Cs`';
        return ret;
  }

  format.formatTimeAgo=function(d) {
            if(d instanceof Date)
                d/=1;
            var t=format.formatTime(Math.floor((_START-d)/1000))
            if(!t)t=''
            var c;
            if(_START-d<24*3600*1000)
                c='L';
            else if(_START-d<2*24*3600*1000)
                c='J';
            else if(_START-d<7*24*3600*1000)
                c='F';
            else if(_START-d<30*24*3600*1000)
                c='D';
            else
                c='c';
            return t+' `'+c+'ago`';
  }
	return {logData, format, AlertError, ConvertTimestamp, Decorrupt, StatusRet}
}
