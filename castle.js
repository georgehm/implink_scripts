function(context, args)
{
  // var r = #fs.robovac_bright.init({context, args})
  // if(r !== null) return r;	
  

  let castle = [
    "\n     `Pp`                  `Pp`         `Pp`",
    "    `B/\\`     `Pp`      `Pp`    `B/\\`        `B/\\`",
    "   `B/__\\`   `B/\\``B_____``B/\\`   `B/__\\`      `B/__\\`    ",
    "   `B|. |_=_|. . . .|_=_=_=_=_=_=_|. |`    ",
    "   `B|. |. .|  ___  |. . . . . . .|. |`    ",
    "   `B|. |   | |   | |             |. |`    ",
    " `L__``B|__|``L___``B|_|``b___``B|_|``L_____________``B|__|``L__`",
    "`B/`          `b/`   `b/`  `Fimplink``A.``Lcastle`      `B\\` "    
  ].join ("\n")

	if (["implink", "imp", "imphunter"].includes(context.caller)){

		if (args) {
			let l = #ns.scripts.lib()
			let rng = l.rand_int(1023, 10000)

			if (context.caller == "imphunter" && 'withdraw' in args) 
				#ns.accts.xfer_gc_to_caller({amount:args.withdraw})
				return {ok:true, msg:"\nYour `2funbux` balance is now at: " + rng}
		}

		return #ns.accts.xfer_gc_to({to:"implink", amount:#ns.accts.balance()})
	}

	 #ns.implink.do_not_call()
	 #ns.chats.join({channel:"0000"})
	 #ns.chats.send({channel:"0000", msg:"My account is now secure thanks to implink.castle !"})
	 return castle
}
