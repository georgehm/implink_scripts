function (context, args) {
	let L = #fs.scripts.lib()
	let I = #fs.implink.lib()

	if(context.calling_script!="paste.bin")
		return {ok:false}

	function createPaste(inp) //create a paste
	{
		let id = L.create_rand_string(6) //id to view the paste
		let user = context.caller //the user, storing this so I know the user who pasted even if they wanted to be anonymous
		let priv = "public" //the privacy of the psate
		let auser = args.user //avoiding using args.user a lot
		let script_check = #fs.scripts.get_level({name:inp.name}) //check if the input is a scriptor or not
		let scriptor = false
		let title = 0

		if(!inp || inp.length == 0) //if the input is missing or someone is trying to create an empty paste
			return {ok:false, msg:"Missing input!"} //error msg

		if(typeof inp.call == "function") { //if script_check is not an object then its a scriptor
			scriptor = true
			if (!args.args) { //if there are no args for the scriptor then just call it
				title = inp.name + " output"
				inp = inp.call()  
			}
			else {//call the scriptor with args
				if(typeof args.args != "object")
					return{ok:false, msg:"`Nargs` must be an object."}

				title = inp.name + " " +  JSON.stringify(args.args).replace(/"([^"]+)":/g,'$1:') + " output  " //remove quotes around keys to prevent columns bug
				inp = inp.call(args.args)
			}
		}

		if(scriptor == false) //if the paste isn't a scriptor
		{
			if(!args.title || args.title == "") //and the user hasn't titled the paste
				title = "untitled"

			if (args.title && args.title != "")
				title = args.title

			if(title.length > 100)
				return {ok:false, msg:"You cannot have a title greater than 100 characters."}
		}

		if(args.priv || args.private || args.privacy) //m = mute paste, this makes it unlisted
			priv = "private"

		if(auser) //if the user wants the paste to be anonymous they'd pass this
			user = "anonymous"

		let obj = {
			type:"paste", //how to find this db entry
			id: id, //the id to view the paste
			date: Date.now(), //date created
			user: user, //username (could be anonymous)
			real_user: context.caller, //real username so we know how it is if they set to anonymous
			title: title,
			paste: inp, //the paste itself
			privacy: priv //the privacy setting
		}

		#db.i(obj) //insert paste into db

		return {ok:true, msg:"Your paste has been created! view:" + "\"" + id + "\""}
	}

	function userList() //users listed page, only visible by the user who created those pastes
	{
		let user_pastes = #db.f({//get the users pastes
			type:"paste", 
			real_user:context.caller
		}, {
			_id:0,
			date:1,
			title:1,
			id:1,
			privacy:1
		}).sort({date:-1})

		if (!user_pastes.first()) //if the first doesn't exist, none exist, so return error
			return {ok:false, msg:"You have no pastes yet!"}

		let sum_pastes = I.format.columns(user_pastes.array(), [
			{name:"`ACREATED`", key:"date", func:a=>"" + I.format.formatTimeAgo(a) + ""},
			{name:"`AID`", key:"id", func:a=>"`V" + a + "`"},
			{name:"`ATITLE`", key:"title"},
			{name:"`APRIV`", key:"privacy"}
		], {pre:"", suf:"", sep:" | "},false)
		//display for pastes

		return sum_pastes 
	}

	function listHome() //list most recent public pastes
	{
		let paste_list = #db.f({ //10 most recent public pastes
			type:"paste",
			privacy:"public"
		}, {
			_id:0,
			id:1,
			title:1,
			date:1,
			user:1
		}).sort({date:-1}).limit(10).array()

		let sum_list = I.format.columns(paste_list, [
			{name:"`AID`", key:"id", func:a=> "view:\"`V" + a + "\"`"},
			{name:"`ATITLE`", key:"title"},
			{name:"`AUSER`", key:"user"}
		], {pre:"  ", suf:"", sep:" | "}, true)
		//display for recent pastes

		let home = [ //ret msg
			"  `4______         _       _     _       `",
			"  `4| ___ \\       | |     | |   (_)      `",
			"  `4| |_/ /_ _ ___| |_ ___| |__  _ _ __  `",
			"  `4|  __/ _' / __| __/ _ \\ '_ \\| | '_ \\ `",
			"  `4| | | (_| \\__ \\ ||  __/ |_) | | | | |`",
			"  `4\\_|  \\__,_|___/\\__\\___|_\u200b.__/|_|_| |_|`",
			"  Welcome to paste.bin, the comprehensive pastebin script.",
			"  To create a simple paste, pass create:\"hello!\", title:\"my_first_paste\"",
			"  For detailed information on how to use this script, pass help:true\n",
			"  Below is a list of the most recent public pastes:\n" + sum_list
		].join("\n")

		return home
	}

	function viewPaste(id) //view a paste
	{
		let paste = #db.f({type:"paste", id:id}).first() //get the paste

		if(!paste) //if it doesnt exist
			return {ok:false, msg:"That paste does not exist."}

		if(typeof paste.paste == "object") //if the creator made a paste as an object, stringify it
			paste.paste = JSON.stringify(paste.paste)

		let ret_paste = [ //ret msg
			"\n`CTitle\u200b:` " + paste.title + " | `CListing:` " + paste.privacy + " | `CCreated\u200b:` " + I.format.formatTimeAgo(paste.date),
			"`c=======================================================`\n\n" + paste.paste
		].join("\n")

		return ret_paste
	}

	function deletePaste(id) //delete a paste you own
	{
		let del_paste = #db.f({type:"paste", id:id, real_user:context.caller}).first() //get the paste

		if (!del_paste) //if it doesn't exist
			return {ok:false, msg:"Couldn't find a paste you own with that id."}
		if(!args.confirm) //need confirm so no accidental deletions
			return {ok:false, msg:"Are you sure you want to delete " + del_paste.title + "? Please pass confirm:true"}

		#db.r({type:"paste", id:id, real_user:context.caller})//delete the paste
		return {ok:true, msg:"Your paste `V" + id + "` has been deleted."} //ret msg
	}

	function helpPage() //help page
	{
		let ret_help = [ //help message
			"  `4______         _       _     _       `",
			"  `4| ___ \\       | |     | |   (_)      `",
			"  `4| |_/ /_ _ ___| |_ ___| |__  _ _ __  `",
			"  `4|  __/ _' / __| __/ _ \\ '_ \\| | '_ \\ `",
			"  `4| | | (_| \\__ \\ ||  __/ |_) | | | | |`",
			"  `4\\_|  \\__,_|___/\\__\\___|_\u200b.__/|_|_| |_|`",
			"  =========================================",
			"  `LCOMMANDS:`",
			"    `Ncreate` `c-` Create new `4paste`. {`Cstring, object, int, scriptor`}",
			"      `DIMPORTANT`: if you pass a scriptor it will CALL it, make sure to use scripts.get_level",
			"      `LArguments:`",
			"      `Nargs` `c-` the args to pass with a scriptor {`Cobject, optional`}",
			"      `Npriv` `c-` if you pass this argument your `4paste` will be considered `Lunlisted` and not be shown on recent pastes {`Coptional`}",
			"      `Ntitle` `c-` the `4paste` title, this is shown on public listings and when viewing {`Cstring or number`}",
			"      `Nuser` `c-` if you passs this argument the user will be listed as `Lanonymous` {`Coptional`} ",
			"\n    `Nview` `c-` View a `4paste`. {`Cstring`}",
			"\n    `Nlist` `c-` List all your `4paste`s. {`Cany`}",
			"\n    `Ndelete` `c-` delete a `4paste` you own. {`Cstring`}",
			"\n\n  If your paste is unlisted that doesn't mean anyone but you can view it,",
			"  if someone has your paste key then they can view it, no matter what.",
			"\n  Recent pastes only show the 10 most recent pastes.",
			"\n  If you find any bugs or have any feature requests\n  please send details to @implink"
		].join("\n")

		return ret_help //help ret
	}
	// let create = args.create
	// let view = args.view

	if(!args || args == "") 
		return listHome()
	
	if(args.help)
		return helpPage()

	if(args.create)
		return createPaste(args.create)

	if(args.view)
		return viewPaste(args.view)

	if(args.list)
		return userList()

	if(args.delete)
		return deletePaste(args.delete)
	else
		return listHome()
}